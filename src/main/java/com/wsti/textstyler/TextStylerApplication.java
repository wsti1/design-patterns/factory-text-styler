package com.wsti.textstyler;

import com.wsti.textstyler.domain.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class TextStylerApplication {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Welcome to TextStyler!");
        System.out.println("Available text tags: paragraph, strong, marked, emphasized");
        System.out.println("\n");

        System.out.print("Enter your text: ");
        String text = scanner.nextLine();

        System.out.print("Enter selected tags separated by space: ");
        String[] tags = scanner.nextLine().split(" ");

        Text result = processText(text, tags);
        System.out.println("\n");
        System.out.println("Result: " + result.getText());
    }

    private static Text processText(String text, String[] tags) {
        TextFactory factory = new TextFactory();
        String[] uniqueTags = reduceDuplicates(tags);

        Text textHolder = new PlainText(text);
        for (String tag : uniqueTags) {
            textHolder = factory.getTaggedText(tag, textHolder);
        }
        return textHolder;
    }

    private static String[] reduceDuplicates(String[] tags) {
        return new HashSet<>(Arrays.asList(tags)).toArray(new String[0]);
    }
}
