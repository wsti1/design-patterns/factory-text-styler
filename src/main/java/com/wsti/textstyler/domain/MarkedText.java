package com.wsti.textstyler.domain;

public class MarkedText extends TextDecorator {

    public MarkedText(Text text) {
        super(text);
    }

    @Override
    public String getText() {
        return "<mark>" + text.getText() + "</mark>";
    }

    @Override
    public void write() {
        System.out.println(this.getText());
    }
}
