package com.wsti.textstyler.domain;

public class PlainText implements Text {

    private String text;

    public PlainText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void write() {
        System.out.println(this.getText());
    }
}
