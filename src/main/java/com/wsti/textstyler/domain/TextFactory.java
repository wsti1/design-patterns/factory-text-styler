package com.wsti.textstyler.domain;

public class TextFactory {

    public Text getTaggedText(String tag, Text source) {
        switch (tag) {
            case "paragraph":
                return new ParagraphText(source);
            case "strong":
                return new StrongText(source);
            case "marked":
                return new MarkedText(source);
            case "emphasized":
                return new EmphasizedText(source);
            default:
                throw new IllegalArgumentException("Tag not recognized: " + tag);
        }
    }
}
