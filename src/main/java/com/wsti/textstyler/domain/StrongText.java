package com.wsti.textstyler.domain;

public class StrongText extends TextDecorator {

    public StrongText(Text text) {
        super(text);
    }

    @Override
    public String getText() {
        return "<strong>" + text.getText() + "</strong>";
    }

    @Override
    public void write() {
        System.out.println(this.getText());
    }
}
